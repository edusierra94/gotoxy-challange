import React, { useEffect, useState } from 'react';
import UserTable from './components/UserTable';
import AddUserForm from './components/AddUserForm';
import EditYo from './components/EditYo';
import { v4 as uuidv4 } from 'uuid';
import axios from "axios";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch } from '@fortawesome/free-solid-svg-icons';
import "bootstrap/dist/css/bootstrap.min.css";
import { useForm, } from 'react-hook-form';

const url = "https://gorest.co.in/public/v1/users";
const urlpro = "https://gorest.co.in/public/v1/users?name="

function App() {

  const { handleSubmit } = useForm();

  const getuser = async () => {

    await axios.get(url).then(response => {
      console.log('hola bb');
      console.log(response.data.data);
      setUsers(response.data.data);
      setTablaUsuarios(response.data.data);


    }).catch(error => {
      console.log(error.message);
    })

  }
  useEffect(() => {
    getuser();
  }, [])

  const [users, setUsers] = useState([])

  // Barra Busqueda  

  const [tablaUsuarios, setTablaUsuarios] = useState([]);

  const filtrar = (terminoBusqueda) => {
    var resultadosBusqueda = tablaUsuarios.filter((elemento) => {
      if (elemento.name.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())
        || elemento.email.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())
        || elemento.gender.toString().toLowerCase().includes(terminoBusqueda.toLowerCase())

      ) {

        return elemento;
      }
    });

    setUsers(resultadosBusqueda);
  }

  const [busqueda, setBusqueda] = useState("");

 
  const handleChange = e => {
    setBusqueda(e.target.value);
    filtrar(e.target.value);
    

  } 

  // soy re piola por buscar con get
  
     const onSubmit = async e => {
      await axios.get(urlpro + busqueda).then (response => {
        console.log(response.data.data);
        setUsers(response.data.data)
       
      }).catch(error => {
        console.log(error.message);
      })
    } 
  //agregar usuario
  const addUser = (user) => {
    user.id = uuidv4()
    console.log(user)
    setUsers([
      ...users,
      user
    ])
  }
  //filtrar usuario

  const HandleChange = event => {

    filtrar(event.target.value);
  }

  // Eliminar usuario
  const deleteUser = id => {
    setUsers(users.filter(user => user.id !== id))
  }

  // Editar usuario
  const [editing, setEditing] = useState(false)

  const initialFormState = { id: null, name: '', useremail: '', gender: '', }
  const [currentUser, setCurrentUser] = useState(initialFormState)

  const editRow = user => {
    setEditing(true)
    setCurrentUser({ id: user.id, name: user.name, useremail: user.useremail, gender: user.gender })
  }

  const updateUser = (id, updatedUser) => {
    setEditing(false)
    setUsers(users.map(user => (user.id === id ? updatedUser : user)))
  }

  return (
    <form onSubmit={handleSubmit(onSubmit)}>
    <div className="alert">
      <button
        className="button muted-button"> Boton Hojota
      </button>
      <div className="flex-row">
        <div className="flex-large">
          {editing ? (
            <div>
              <h2>Editar usuario</h2>
              <EditYo
                setEditing={setEditing}
                currentUser={currentUser}
                updateUser={updateUser}
              />
            </div>
          ) : (
            <div>
              <h2>Agregar usuario</h2>
              <AddUserForm addUser={addUser} />
            </div>
          )}
        </div>

        <div
          className="flex-large">
          <div className="containerInput">
          
            <input
              className="form-control inputBuscar"
              value={busqueda}
              placeholder="buscar "
              onChange={handleChange}
               />
            <button
              type="submit"
              value= {busqueda}
              onClick= {handleSubmit}
              className="btn btn-sucess">
              <FontAwesomeIcon icon={faSearch} />
              Buscatesta
            </button>

            <h2>Lista de Usuarios</h2>

            <div className="select">
              <select onChange={HandleChange} id="standard-select">
                <option value="male"
                >Pene </option>
                <option value="female"
                >Vagina</option> </select> </div>
            <UserTable users={users} deleteUser={deleteUser}
              editRow={editRow} />
          </div>
        </div>
      </div>

    </div>
    </form>
  );
}

export default App;
