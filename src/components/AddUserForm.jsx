import axios from 'axios';
import React from 'react';
import { useForm, } from 'react-hook-form'


const url = "https://gorest.co.in/public/v1/users";
const keytoken = "db11d028aa701f218e550b641665bea58a3eb0dc3207ae9607a6efcbf8cd923b";
const config = {
    headers: { Authorization: `Bearer ${keytoken}` }
};



const AddUserForm = (props) => {
    const { register, errors, handleSubmit } = useForm();


    const onSubmit = (data, e) => {
        const bodyParameters = {
            name: data.name,
            email: data.email,
            gender: data.gender,
            status: "active" 
        } 
          console.log(bodyParameters)

         

        data.id = null 
         props.addUser(data)
        e.target.reset();
        axios.post(url, bodyParameters, config)};
        

    return (
        <form onSubmit={handleSubmit(onSubmit)}>
            <label>Nombre</label>
            <input
                type="text"
                name="name"
                ref={register({ required: { value: true, message: 'Valor requerido' } })}
            />
            <div>
                {errors?.name?.message}
            </div>
            <label>Email</label>
            <input
                type="text"
                name="email"
                ref={register({ required: { value: true, message: 'Valor requerido' } })}
            />
            <div>
                <label>Genero</label>
                <input
                    type="text"
                    name="gender"
                    ref={register({ required: { value: true, message: 'Valor requerido' } })}
                />
                <div>
                    {errors?.name?.message}
                </div>
                {errors?.username?.message}
            </div>
            <button type="submit">Agregar Usuario</button>
        </form>
    );
}

export default AddUserForm;